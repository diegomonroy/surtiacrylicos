<!-- Begin Content -->
	<section class="content full-width" style="padding: 0;" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<div class="title">
					<h2 class="text-center">NUESTROS PRODUCTOS</h2>
				</div>
			</div>
		</div>
	</section>
	<section class="content" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->