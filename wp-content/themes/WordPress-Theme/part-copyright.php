<!-- Begin Copyright -->
	<section class="copyright text-center" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'colombia' ); ?>
			</div>
			<div class="small-12 medium-6 columns">
				&copy; <?php echo date( 'Y' ); ?> <a href="<?php echo site_url(); ?>"><?php bloginfo('name'); ?></a>. Todos los derechos reservados. Sitio desarrollado por <a href="http://www.amapolazul.com" target="_blank">Amapolazul</a>.
			</div>
			<div class="small-12 medium-3 columns"></div>
		</div>
	</section>
<!-- End Copyright -->