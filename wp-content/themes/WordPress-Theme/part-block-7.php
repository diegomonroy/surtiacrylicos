<!-- Begin Block 7 -->
	<section class="block_7" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'block_7' ); ?>
			</div>
		</div>
	</section>
<!-- End Block 7 -->