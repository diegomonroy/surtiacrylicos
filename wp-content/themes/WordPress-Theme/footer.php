		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-1' ); endif; ?>
		<?php /* if ( is_front_page() || is_page( array( 'historia', 'nuestras-sedes', 'nuestro-equipo-de-trabajo', 'politicas', 'nuestros-productos' ) ) ) : get_template_part( 'part', 'block-2' ); endif; */ ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-3' ); endif; ?>
		<?php if ( is_page( array( 'historia', 'nuestras-sedes', 'nuestro-equipo-de-trabajo', 'politicas', 'nuestros-productos' ) ) ) : get_template_part( 'part', 'block-6' ); endif; ?>
		<?php get_template_part( 'part', 'block-7' ); ?>
		<?php get_template_part( 'part', 'block-4' ); ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'block-5' ); endif; ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php get_template_part( 'part', 'chat' ); ?>
		<?php wp_footer(); ?>
	</body>
</html>