<!-- Begin Content -->
	<section class="content landing" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
				<?php dynamic_sidebar( 'block_landing_5' ); ?>
				<?php dynamic_sidebar( 'block_landing_6' ); ?>
			</div>
		</div>
	</section>
<!-- End Content -->