<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/foundation/js/vendor/jquery.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/foundation/js/vendor/what-input.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/wow/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
			'product-menu' => __( 'Product Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Top 1',
			'id' => 'top_1',
			'before_widget' => '<div class="moduletable_to11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Menu',
			'id' => 'menu',
			'before_widget' => '<div class="moduletable_to2">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search',
			'id' => 'search',
			'before_widget' => '<div class="moduletable_to3">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 5',
			'id' => 'block_5',
			'before_widget' => '<div class="moduletable_b51">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 6',
			'id' => 'block_6',
			'before_widget' => '<div class="moduletable_b61">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 7',
			'id' => 'block_7',
			'before_widget' => '<div class="moduletable_b71">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Colombia',
			'id' => 'colombia',
			'before_widget' => '<div class="moduletable_co1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block Landing 5',
			'id' => 'block_landing_5',
			'before_widget' => '<div class="block-landing-5">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block Landing 6',
			'id' => 'block_landing_6',
			'before_widget' => '<div class="block-landing-6">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Functions to declare WooCommerce image sizes
 */
function woocommerce_catalog_image( $size ) {
	return array(
		'width' => 300,
		'height' => 300,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_thumbnail', 'woocommerce_catalog_image' );

function woocommerce_single_image( $size ) {
	return array(
		'width' => 600,
		'height' => 600,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_single', 'woocommerce_single_image' );

function woocommerce_gallery_image( $size ) {
	return array(
		'width' => 200,
		'height' => 200,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'woocommerce_gallery_image' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 20;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Custom hooks in WooCommerce
 */
function add_query_vars_filter( $vars ) {
	$vars[] = 'cotice_producto';
	return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

function custom_description() {
	$post = get_post( $post );
	echo '
		<p class="text-center">
			<!--<a href="' . get_site_url() . '/cotice-aqui/?cotice_producto=' . $post->post_title . '" class="button">Cotice Aquí</a>-->
			<a href="https://api.whatsapp.com/send?phone=573182144440&amp;text=Buen%20día,%20me%20gustaría%20recibir%20más%20información%20sobre%20' . $post->post_title . '" target="_blank" class="button whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i> Contacta a una asesora</a>
		</p>
	';
	the_content();
	echo '
		<div class="clear"></div>
		<p class="text-center">
			<a href="https://api.whatsapp.com/send?phone=573507735947&amp;text=Buen%20día,%20me%20gustaría%20recibir%20más%20información%20sobre%20' . $post->post_title . '" target="_blank" class="button whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i> Contacta a una asesora</a>
		</p>
		<!--<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>-->
	';
}
add_action( 'woocommerce_single_product_summary', 'custom_description', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

function remove_description_tab( $tabs ) {
	if ( isset( $tabs['description'] ) ) unset( $tabs['description'] );
	return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'remove_description_tab', 20, 1 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function custom_clear() {
	$post = get_post( $post );
	echo '
		<div class="clear"></div>
		<p class="text-center">
			<a href="https://api.whatsapp.com/send?phone=573023943183&amp;text=Buen%20día,%20me%20gustaría%20recibir%20más%20información%20sobre%20' . $post->post_title . '" target="_blank" class="button whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i> Contacta a una asesora</a>
		</p>
		<!--<div class="text-center">
			<button class="hollow button" onclick="window.history.go(-1);">Volver</button>
		</div>-->
	';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_clear', 10 );

function wc_remove_product_schema_product_archive() {
	remove_action( 'woocommerce_shop_loop', array( WC()->structured_data, 'generate_product_data' ), 10, 0 );
}
add_action( 'woocommerce_init', 'wc_remove_product_schema_product_archive' );