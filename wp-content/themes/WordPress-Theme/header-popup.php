<!DOCTYPE html>
<html class="no-js" lang="<?php echo get_locale(); ?>">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="ja5GworoI2aagoM2sgC4HpnOcXEqPqxbgsNpL3ucfQ0">
		<title><?php bloginfo('title'); ?></title>
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo site_url(); ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php bloginfo('name'); ?>">
		<meta property="og:description" content="<?php bloginfo('description'); ?>">
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/build/logo_ogp.png">
		<link rel="image_src" href="<?php echo get_template_directory_uri(); ?>/build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<?php wp_head(); ?>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/build/favicon.ico">
		<!-- Begin Google Analytics -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-39034949-1', 'auto');
			ga('send', 'pageview');
		</script>
		<!-- End Google Analytics -->
		<!-- Begin Google Tag Manager -->
		<script>
			(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-MT55M95');
		</script>
		<!-- End Google Tag Manager -->
		<!-- Begin Google Tag Manager (noscript) -->
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MT55M95" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<?php
			if (
				is_page( array(
					'acrilicos-bogota',
					'carteleras-corporativas',
					'atriles-en-acrilico',
					'fabricantes-de-acrilicos',
					'acrilicos-colombia',
					'divisiones-en-acrilico',
					'laminas-de-acrilico',
					'tableros-acrilicos',
					'placas-en-acrilico',
					'trofeos-y-medallas-en-acrilico',
					'exhibidores',
					'habladores-acrilico',
					'tableros-de-baloncesto',
					'caretas-en-acrilico',
					'senaletica-corporativa',
					'buzones-en-acrilico',
					'trabajos-en-acrilico'
				) )
			) :
		?>
		<!-- Google Tag Manager -->
		<script>
			(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-KV3ZQGR');
		</script>
		<!-- End Google Tag Manager -->
		<?php endif; ?>
	</head>
	<body>
		<?php
			if (
				is_page( array(
					'acrilicos-bogota',
					'carteleras-corporativas',
					'atriles-en-acrilico',
					'fabricantes-de-acrilicos',
					'acrilicos-colombia',
					'divisiones-en-acrilico',
					'laminas-de-acrilico',
					'tableros-acrilicos',
					'placas-en-acrilico',
					'trofeos-y-medallas-en-acrilico',
					'exhibidores',
					'habladores-acrilico',
					'tableros-de-baloncesto',
					'caretas-en-acrilico',
					'senaletica-corporativa',
					'buzones-en-acrilico',
					'trabajos-en-acrilico'
				) )
			) :
		?>
		<!-- Google Tag Manager (noscript) -->
		<noscript>
			<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KV3ZQGR"
					height="0" width="0" style="display:none;visibility:hidden">
			</iframe>
		</noscript>
		<!-- End Google Tag Manager (noscript) -->
		<?php endif; ?>